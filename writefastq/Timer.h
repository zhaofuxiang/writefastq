//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#ifndef TIMER_H
#define TIMER_H

#pragma once

#include <thread>
#include <mutex>  
using namespace std;

class Timer
{
public:
    Timer()
        : t1(res::zero())
        , t2(res::zero())
    {
        tic();
    }

    ~Timer()
    {}

    void tic()
    {
        t1 = clock::now();
    }

    double toc(int div=1000)
    {
        t2 = clock::now();
        double duration = chrono::duration_cast<res>(t2 - t1).count() / 1e3;
        if (duration / 1e3 < 0.1)
            duration = 0.0;
        tic();
        return duration / div;
    }

private:
    typedef chrono::high_resolution_clock clock;
    typedef chrono::microseconds res;

    clock::time_point t1;
    clock::time_point t2;
};

#endif // TIMER_H
