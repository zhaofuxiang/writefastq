#ifndef DATA_STRUCTURE_H
#define DATA_STRUCTURE_H
#pragma once

#include <vector>
#include <string>
#include <map>
using std::vector;
using std::string;
using std::map;

typedef unsigned short u_short;

/* 
---------------------------------------- writefastq input parameters ------------------------
*/
struct splitInfo
{
	u_short  start_cycle;
	u_short  length;
	u_short  mismatch;
};

struct barcodeInfo
{
	bool split_barcode; // true: just split it false: do not split barcode
	map<string, string> barcode_map;
	vector<splitInfo> split_info;
	bool reverse; // true: reverse false: not reverse
};

struct fovInfo
{
	u_short col;
	u_short row;
};

struct calInfo
{
	u_short filter_mode; // 0->no filter 1->do filter others means specific filter method
	u_short filter_quality_read1; // filter quality threshold
	u_short filter_quality_read2;
	string cal_path;
	vector<fovInfo> fov_list;

	u_short end_cycle_process_read1; // process for end cycle, so ignore end cycle
	u_short end_cycle_process_read2;

	bool use_gpu; // true: use gpu false: use cpu
};

struct fastqInfo
{
	string fastq1;
	string fastq2;
};

struct fastqParameters
{
	barcodeInfo barcode_info;

	u_short  read1_length;
	u_short  read2_length;

	u_short thread_number; // the max cpu number the user wanted
	u_short memory; // the max memory the user wanted, unit is (GB)

	string out_path; // output fastq directory

	string read_prefix; // like: FS2000L1 (slide+lane), used for out fastq

	u_short data_mode; // 0->read data from cal files 1->read data from fastq files
	calInfo cal_info;
	fastqInfo fastq_info;
};

/*
---------------------------------------- write fastq data structure ------------------------
*/

enum DataFlag
{
	empty,
	hold,
	raw,
	transform,
	split
};

struct DataInfo
{
	int cycle;
	int base_number;
	int col;
	int row;
	unsigned char *data;
	std::string sequence;
	std::string score;
	DataFlag data_flag;
};



#endif // DATA_STRUCTURE_H
