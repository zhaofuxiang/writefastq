
#include <iostream>
#include <vector>
using namespace std;

//#include "../../logger/Logging.hpp"

//namespace CGI {
//	namespace OSIIP {
		/*namespace
		{
			Log::Logger gpuLog("pipeline.framework.processor.writefq");
		}*/

		template< typename T >
		void myCheck(T result, char const *const func, const char *const file, int const line)
		{
			if (result)
			{
				//CGI_ERROR(gpuLog, "CUDA error at " << file << ":" << line << "code=" << static_cast<unsigned int>(result) << "(" << _cudaGetErrorEnum(result) << ")" << "\""<<func<<"\"");
				std::runtime_error error("cuda error");
				throw std::exception(error);
			}
		}
		// This will output the proper CUDA error strings in the event that a CUDA host call returns an error
		#define myCheckCudaErrors(val)           myCheck ( (val), #val, __FILE__, __LINE__ )

		int initGPUWFqMEM(const int calSize, const int numCycles, int *base2int, int *filterParam, const char* idx2Base, std::vector<int> &hashTable);
		void transGPU(char *seqData, char *scoreData, unsigned char *inData, const int numCycles, const int numDNB, const int filterCycStart2, int* &h_hashVal, int* &h_speciesNo, int startBarcodePos, int barcodeLen);
		void destroyGPUWFqMEM();

//	}
//}