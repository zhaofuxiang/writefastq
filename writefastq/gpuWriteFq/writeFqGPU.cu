
//#include "../MacroDefine.hpp"
#include "writeFqGPU.cuh"
#include <device_launch_parameters.h>
#include <helper_cuda.h>
//#include <helper_functions.h>
#include <cuda_runtime.h>

#define START_QUAL 33

//namespace CGI {
//	namespace OSIIP {

#define TILE_DIM 128
#define BLOCK_ROWS 8

		/*************GPU malloc memory***************/
		char *d_char, *h_char;//base and score
		unsigned char *d_uchar, *h_uchar;//cal file data
		int *d_hashVal, *d_speciesNo;

		//GPU parameter threads shared
		int *d_gpuParam;
		int *d_hashTable;
		char *d_idx2Base;//index->base
		/*********************************************/

		__global__ void transKernel(char *seqData, char *scoreData, unsigned char *inData, const int width, const int height, char *d_Idx2Base, int *d_base2Idx)
		{
			__shared__ unsigned char tile[TILE_DIM][TILE_DIM + 1];
			int ciIndex = blockIdx.x * TILE_DIM + threadIdx.x;
			int riIndex = blockIdx.y * TILE_DIM + threadIdx.y;
			int coIndex = blockIdx.y * TILE_DIM + threadIdx.x;
			int roIndex = blockIdx.x * TILE_DIM + threadIdx.y;
			int index_in = sizeof(int) + ciIndex + (riIndex)* (width + sizeof(int));
			int index_out = coIndex + (roIndex)* height;

			unsigned char tmpBuff;
			char Qscore, Base;
			int i;

#pragma unroll
			for (i = 0; i < TILE_DIM; i += BLOCK_ROWS)
				if ((ciIndex < width) && (riIndex + i < height))
					tile[threadIdx.y + i][threadIdx.x] = inData[index_in + i * (width + sizeof(int))];
			__syncthreads();

#pragma unroll
			for (i = 0; i < TILE_DIM; i += BLOCK_ROWS)
				if ((coIndex < height) && (roIndex + i < width))
				{
					tmpBuff = tile[threadIdx.x][threadIdx.y + i];
					Qscore = (tmpBuff & 0x3f) + START_QUAL;
					Base = 'N';
					if (Qscore > START_QUAL)
						Base = d_Idx2Base[(tmpBuff >> 6)];
					seqData[index_out + i*height] = Base;
					scoreData[index_out + i*height] = Qscore;
				}
			__syncthreads();
		}


		__global__ void mergeSeqBigCal(unsigned char *d_filterFlag, char *d_seqData, char *d_scoreData, int *d_filterParam, int d_NCyc, int d_NDnb, int m_read1)
		{
			int idnb = (blockIdx.x*blockDim.x) + threadIdx.x;
			if (idnb < d_NDnb)
			{
				d_filterFlag[idnb] = 0;

				int filterCyc, Qdel;
				char Qscore;
				for (size_t k = 0; k < d_NCyc; k++)
				{
					filterCyc = d_filterParam[k];
					Qdel = d_filterParam[d_NCyc + k];
					Qscore = d_scoreData[idnb*d_NCyc + k];

					if (k == m_read1 && d_filterFlag[idnb] <= 2)
					{
						d_filterFlag[idnb] = 0;
					}

					if (filterCyc && (Qscore < START_QUAL + Qdel)) //get filter from quality, second param is Qdel
					{
						d_filterFlag[idnb] += 1;//use d_seqSpecies for m_filterFlag to save memory
												//printf("thread_%d:%d \n", idnb, Qscore);
					}
				}
			}
		}

		__global__ void filterAndBarcode(int *d_speciesNo, int *d_hashVal, unsigned char *d_filterFlag, char *d_seqData, char *d_scoreData, int *d_filterParam, int d_NCyc, int d_NDnb, int m_read1,
			int *d_hashTable, int *d_base2Idx, int startBarcodePos, int barcodeLen)
		{
			int idnb = (blockIdx.x*blockDim.x) + threadIdx.x;
			if (idnb < d_NDnb)
			{
				char bar;
				int speciesNO = 0;
				int H_value = 0;
				int filterCyc, Qdel;
				char Qscore;
				for (size_t k = 0; k < d_NCyc; k++)
				{
					filterCyc = d_filterParam[k];
					Qdel = d_filterParam[d_NCyc + k];
					Qscore = d_scoreData[idnb*d_NCyc + k];

					if (k == m_read1 && d_filterFlag[idnb] <= 2)
					{
						d_filterFlag[idnb] = 0;
					}

					if (filterCyc && (Qscore < START_QUAL + Qdel)) //get filter from quality, second param is Qdel
					{
						d_filterFlag[idnb] += 1;//use d_seqSpecies for m_filterFlag to save memory
												//printf("thread_%d:%d \n", idnb, Qscore);
					}

					if ((k >= startBarcodePos) && (k < (startBarcodePos + barcodeLen)))
					{
						bar = d_seqData[idnb*d_NCyc + k];
						H_value = H_value * 5 + d_base2Idx[bar];
					}
				}

				if (barcodeLen > 0)
				{
					speciesNO = d_hashTable[H_value];
					if (speciesNO == -1)
						speciesNO = 0;
				}
				d_hashVal[idnb] = H_value;
				d_speciesNo[idnb] = speciesNO;
			}
			//if (idnb < 100)
			//{
			//	printf("seq[%d]:%c, d_hashVal:%d\n", idnb, d_seqData[idnb*d_NCyc], d_hashVal[idnb]);
			//}
		}

		int initGPUWFqMEM(const int calSize, const int numCycles, int *base2int, int *filterParam, const char* idx2Base, std::vector<int> &hashTable)
		{
			myCheckCudaErrors(cudaSetDevice(0));
			//gpuLog.prefix() << "writefq: ";

			size_t free_byte, total_byte;
			cudaError_t cuda_status;
			cuda_status = cudaMemGetInfo(&free_byte, &total_byte);
			if (cudaSuccess != cuda_status) {
				//CGI_ERROR(gpuLog, "Error: cudaMemGetInfo fails: " << cudaGetErrorString(cuda_status));
				return -1;
			}
			//CGI_INFO(gpuLog, "GPU total memroy: "<< (int)(total_byte / pow(1024, 2)) << "(MB) free memory: " << (int)(free_byte / pow(1024, 2)) << "(MB)");

			size_t memGPUPub = sizeof(int)*(128 + numCycles * 3);
			size_t memGPUPerThr = (calSize + sizeof(int))*(numCycles * 3);
			//CGI_INFO(gpuLog, "GPU memory need: " << (int)((memGPUPerThr + memGPUPub) / pow(1024, 2)) << "(MB)");

			if ((memGPUPerThr + memGPUPub) > free_byte)
			{
				//CGI_ERROR(gpuLog, "GPU memory error, WriteFq lack of GPU memory");
				return -1;
			}

			try
			{
				myCheckCudaErrors(cudaMalloc((void**)&d_idx2Base, sizeof(char) * 6));
				myCheckCudaErrors(cudaMalloc((void**)&d_char, sizeof(char)*numCycles * calSize * 2));
				myCheckCudaErrors(cudaMalloc((void**)&d_uchar, sizeof(unsigned char)*(sizeof(int) + calSize)*numCycles));
				myCheckCudaErrors(cudaMalloc((void**)&d_hashVal, sizeof(int)*calSize));
				myCheckCudaErrors(cudaMalloc((void**)&d_speciesNo, sizeof(int)*calSize));
				myCheckCudaErrors(cudaMalloc((void**)&d_gpuParam, sizeof(int)*(128 + numCycles * 3)));
				myCheckCudaErrors(cudaMemcpy(d_gpuParam, base2int, sizeof(int) * 128, cudaMemcpyHostToDevice));


				myCheckCudaErrors(cudaMalloc((void**)&d_hashTable, sizeof(int)*hashTable.size()));
				myCheckCudaErrors(cudaMemcpy(d_hashTable, hashTable.data(), sizeof(int) * hashTable.size(), cudaMemcpyHostToDevice));

				myCheckCudaErrors(cudaMemcpy(d_gpuParam + 128, filterParam, sizeof(int) * numCycles * 3, cudaMemcpyHostToDevice));
				//myCheckCudaErrors(cudaMemcpy(d_gpuParam+128+m_numCycles*3, barcodeHash.data(), sizeof(int) * barcodeHash.size(), cudaMemcpyHostToDevice));
				//myCheckCudaErrors(cudaMalloc((void**)&d_idx2Base, sizeof(char) * 6));
				//myCheckCudaErrors(cudaMemcpy(d_idx2Base, BASE, sizeof(char) * 6, cudaMemcpyHostToDevice));
				myCheckCudaErrors(cudaMemcpy(d_idx2Base, idx2Base, sizeof(char) * 6, cudaMemcpyHostToDevice));
			}
			catch (exception)
			{
				//CGI_ERROR(gpuLog, "There is cuda error in function initGPUWFqMEM()");
				return -1;
			}
			return 0;
		}


		void transGPU(char *seqData, char *scoreData, unsigned char *inData, const int numCycles, const int numDNB, const int filterCycStart2, int* &h_hashVal, int* &h_speciesNo, int startBarcodePos, int barcodeLen)
		{
			checkCudaErrors(cudaSetDevice(0));
			char *d_seqData = d_char;
			char *d_scoreData = d_seqData + numDNB*numCycles;
			int ucharOffset = (numDNB + sizeof(int))*numCycles;
			unsigned char *d_buffData = d_uchar;
			unsigned char *d_filterFlag = d_buffData;
			//unsigned char *d_speciesNo = d_buffData + numDNB;

			int *d_base2int = d_gpuParam;
			int *d_filterParam = d_gpuParam + 128;

			/*checkCudaErrors(cudaMallocHost(&h_hashVal, sizeof(int)*numDNB));
			checkCudaErrors(cudaMallocHost(&h_speciesNo, sizeof(int)*numDNB));*/

			checkCudaErrors(cudaMemcpy(d_buffData, inData, sizeof(unsigned char)*(numDNB + sizeof(int))*numCycles, cudaMemcpyHostToDevice));

//#ifdef BIG_CAL
			dim3 grid((numDNB + TILE_DIM - 1) / TILE_DIM, (numCycles + TILE_DIM - 1) / TILE_DIM), threads(TILE_DIM, BLOCK_ROWS);
			transKernel << <grid, threads >> > (d_seqData, d_scoreData, d_buffData, numDNB, numCycles, d_idx2Base, d_base2int);
			checkCudaErrors(cudaThreadSynchronize());

			checkCudaErrors(cudaMemset(d_filterFlag, 0, numDNB));

			filterAndBarcode << <(numDNB + 1023) / 1024, 1024 >> > (d_speciesNo, d_hashVal, d_filterFlag, d_seqData, d_scoreData, d_filterParam, numCycles, numDNB, filterCycStart2,
				d_hashTable, d_base2int, startBarcodePos, barcodeLen);
			checkCudaErrors(cudaDeviceSynchronize());
			//cout << "GPU----- merge and filter use time:" << difftime(time(NULL), start) << "s.\n\n";
//#else
			//mergeSeqSmallCal << <CEIL(numDNB / 1024.0f), 1024 >> > (d_seqData, d_scoreData, d_buffData, numCycles, numDNB, d_int2base);
//#endif

			checkCudaErrors(cudaMemcpy(seqData, d_seqData, sizeof(char)*numCycles * numDNB, cudaMemcpyDeviceToHost));
			checkCudaErrors(cudaMemcpy(scoreData, d_scoreData, sizeof(char)*numCycles * numDNB, cudaMemcpyDeviceToHost));
			checkCudaErrors(cudaMemcpy(inData, d_filterFlag, sizeof(unsigned char) * numDNB, cudaMemcpyDeviceToHost));

			checkCudaErrors(cudaMemcpy(h_speciesNo, d_speciesNo, sizeof(int) * numDNB, cudaMemcpyDeviceToHost));
			/*printf("size %d\n", (numDNB / 4 + 375));
			for (int i = 0; i < numDNB; ++i)
				printf("i %d %d\n",i, h_speciesNo[i]);*/
				//checkCudaErrors(cudaMemcpy(h_speciesNo, d_speciesNo, sizeof(int) * (numDNB/4 + 375), cudaMemcpyDeviceToHost));

			checkCudaErrors(cudaMemcpy(h_hashVal, d_hashVal, sizeof(int) * numDNB, cudaMemcpyDeviceToHost));

		}


		void destroyGPUWFqMEM()
		{
			checkCudaErrors(cudaSetDevice(0));
			checkCudaErrors(cudaFree(d_char));
			checkCudaErrors(cudaFree(d_uchar));
			checkCudaErrors(cudaFree(d_gpuParam));
			checkCudaErrors(cudaFree(d_idx2Base));
			checkCudaErrors(cudaFree(d_hashVal));
			checkCudaErrors(cudaFree(d_speciesNo));
			checkCudaErrors(cudaFree(d_hashTable));
			//checkCudaErrors(cudaFreeHost(h_char));
			//checkCudaErrors(cudaFreeHost(h_uchar));
		}

//	}
//}
