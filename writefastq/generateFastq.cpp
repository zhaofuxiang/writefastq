
#include "generateFastq.h"
#include "writeFastq.h"

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include <boost/filesystem.hpp>

int generateFastq(const fastqParameters& fqPara)
{
	// int WriteFastq instance
	WriteFastq wf(fqPara);
	int calSize = 1600225;

	wf.init(calSize);

	thread output_data_thread(boost::bind(&WriteFastq::outData, &wf));
	// travel all cal files
	vector<fovInfo> fov_list = fqPara.cal_info.fov_list;
	string cal_path = fqPara.cal_info.cal_path;

	int read_thread_number = fqPara.thread_number;
	boost::asio::io_service ioService;
	boost::thread_group threadpool;
	{
		boost::asio::io_service::work work(ioService);
		for (int i = 0; i < read_thread_number; ++i)
			threadpool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));

		for (vector<fovInfo>::const_iterator it = fov_list.begin(); it != fov_list.end(); ++it)
		{
			char cal_file_name[20];
			sprintf(cal_file_name, "/C%03dR%03d.cal", it->col, it->row);

			std::string filename(cal_path);
			filename += cal_file_name;
			if (!boost::filesystem::exists(filename))
				continue;
			// wf.getData(filename, it->col, it->row);
			ioService.post(boost::bind(&WriteFastq::getData, &wf, filename, it->col, it->row));
			this_thread::sleep_for(chrono::microseconds(100));
		}

	}

	threadpool.join_all();
	
	
	wf.process_finished = true;

	output_data_thread.join();

	wf.endFastq();

	return 0;
}