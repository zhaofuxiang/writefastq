//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#ifndef BARCODE_H
#define BARCODE_H

#include <string>
#include <vector>
#include <iostream>

#include <mutex>
#include <unordered_map>
using namespace std;

//#define DEBUG

class Barcode
{
public:
    Barcode(){}
    Barcode(std::string & barcodeFile);
    ~Barcode();

    void reverseComplement();
	void createMisDict(vector<int> & barcode_info);
    vector<string> getBarcodes();
    int searchBarcdeNumber(string & seq);
    int statInfo(const string & path);


private:
    void load(string & barcodeFile);
    void makeHash(vector<string>& sequence, int species, unordered_map<string,int>& hash_table);
    
public:
	int lens;

private:
    int count;
    unordered_map<string, int> mis_dict;
    vector<pair<string,string>> barcodes;
    int m_barcode_num;
    mutex * m_barcodeInfoLock;
    unordered_map<string, pair<int, long long> > m_barcodeInfo; // key is sequence, value is <species, times>
    
};

#endif // BARCODE_H
