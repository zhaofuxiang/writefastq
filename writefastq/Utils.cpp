//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#include "Utils.h"
#include <algorithm>

#include <iostream>
#include <iterator>
#include <sstream>

#include <boost/filesystem.hpp>


std::vector<std::string> split_string(std::string & s, std::string sep)
{
    std::vector<std::string> r;
    int seq_pos;
    std::string line;
    while ((seq_pos = s.find(sep)) != std::string::npos)
    {
        line = s.substr(0, seq_pos);
        r.push_back(line);
        s = s.substr(seq_pos+1, s.size());
    }

    return r;
}


void split(const std::string &str, char delim, std::vector<std::string> &elems, bool skip_empty)
{
    std::istringstream iss(str);
    for (std::string item; getline(iss, item, delim); )
        if (skip_empty && item.empty()) continue;
        else elems.push_back(item);
}

std::string replace_string(std::string s, const std::string k, const std::string v)
{
    std::string r;

    //std::transform(s.begin(), s.end(), r.begin(), )
    int pos;
    for (int i = s.size()-1; i >= 0; --i)
    {
        if ((pos = k.find(s[i])) != std::string::npos)
            r.append(v, pos, 1);
        else
            r.append(s, i, 1);
    }
    return r;
}

bool create_directory(std::string &d)
{
	namespace fs = boost::filesystem;
	fs::path dir(d);
	
	try {
		fs::create_directories(dir);
		return true;
	}
	catch (const fs::filesystem_error& e) {
		std::cout<< "create director " << d << " failed. "
			<< " error: " << e.what() << std::endl;
		return false;
	}
}

#ifdef _WIN32
mylong getMemoryInfo(mylong &totalPhysMem, mylong &validPhysMem)
{
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof(statex);
	GlobalMemoryStatusEx(&statex);

	//statex.ullTotalPhys; // All physicial memery
	//statex.ullAvailPhys; // Avilide physicial memery
	//statex.ullTotalPageFile;
	//statex.ullTotalVirtual; // Virtual memery
	//statex.ullAvailVirtual; // Virtual memery can be used

	//// ?????
	//printf("There is  %ld percent of memory in use.\r\n", (int)statex.dwMemoryLoad);
	//
	//// ????
	//printf("There are %I64d total Mbytes of physical memory.\r\n", (int)(statex.ullTotalPhys / pow(1024,2)));
	//
	//// ??????
	//printf("There are %I64d free Mbytes of physical memory.\r\n", (int)(statex.ullAvailPhys / pow(1024, 2)));
	//
	//// ???????
	//printf("There are %I64d total Mbytes of paging file.\r\n", (int)(statex.ullTotalPageFile / pow(1024, 2)));
	//
	//// ???????
	//printf("There are %I64d free Mbytes of paging file.\r\n", (int)(statex.ullAvailPageFile / pow(1024, 2)));
	//
	//// ????
	//printf("There are %I64d total Mbytes of virtual memory.\r\n", (int)(statex.ullTotalVirtual / pow(1024, 2)));

	//// ??????
	//printf("There are %I64d free Mbytes of virtual memory.\r\n", (int)(statex.ullAvailVirtual / pow(1024, 2)));

	//// ullAvailExtendedVirtual????
	//printf("There are %I64d free Mbytes of extended memory.\r\n", statex.ullAvailExtendedVirtual / 1024);
	//m_staticMemory.SetWindowText(strInfo);

	totalPhysMem = (mylong)(statex.ullTotalPhys);
	validPhysMem = (mylong)(statex.ullAvailPhys);

	return (mylong)(statex.ullTotalPageFile);
}

#else//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
mylong getMemoryInfoBackup(mylong &totalPhysMem, mylong &validPhysMem)
{
	struct sysinfo si;
	sysinfo(&si);
	totalPhysMem = (mylong)(si.totalram);
	validPhysMem = (mylong)(si.freeram);

	//printf("Totalram:       %d\n", si.totalram);
	//printf("Available:      %d\n", si.freeram);

	//struct sysinfo {
	//	long uptime;             /* Seconds since boot */
	//unsigned long loads[3];  /* 1, 5, and 15 minute load averages */
	//unsigned long totalram;  /* Total usable main memory size */
	//unsigned long freeram;   /* Available memory size */
	//unsigned long sharedram; /* Amount of shared memory */
	//unsigned long bufferram; /* Memory used by buffers */
	//unsigned long totalswap; /* Total swap space size */
	//unsigned long freeswap;  /* swap space still available */
	//unsigned short procs;    /* Number of current processes */
	//unsigned long totalhigh; /* Total high memory size */
	//unsigned long freehigh;  /* Available high memory size */
	//unsigned int mem_unit;   /* Memory unit size in bytes */
	//char _f[20 - 2 * sizeof(long) - sizeof(int)]; /* Padding for libc5 */
	//};
	return totalPhysMem;
}

mylong getMemoryInfo(mylong &totalPhysMem, mylong &validPhysMem)
{
	FILE *fp;
	int i, j;
	char total[20];
	char free[20];
	char temp[100];
	if ((fp = fopen("/proc/meminfo", "r")) == NULL)
	{
		printf("open /proc/meminfo error.\n");
		return getMemoryInfoBackup(totalPhysMem, validPhysMem);
	}
	else
	{
		for (i = 0; i<3; i++)
		{
			fgets(temp, 100, fp);
			if (i == 0) //total info at 1 row
				strcpy(total, temp);
			if (i == 2) //free info at 3 row
				strcpy(free, temp);
		}
	}
	if (strlen(total)>0)
	{
		for (i = 0, j = 0; i<strlen(total); i++)
		{
			if (isdigit(total[i]))
				total[j++] = total[i];
		}
		total[j] = '\0';
	}
	if (strlen(free)>0)
	{
		for (i = 0, j = 0; i<strlen(free); i++)
		{
			if (isdigit(free[i]))
				free[j++] = free[i];
		}
		free[j] = '\0';
	}

	totalPhysMem = (mylong)(atol(total) * 1024);
	validPhysMem = (mylong)(atol(free) * 1024);
	return totalPhysMem;
}

#endif

int get_CPU_core_num()
{
#if defined(WIN32)
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	return info.dwNumberOfProcessors;
#elif defined(linux) || defined(SOLARIS) || defined(AIX)
	return get_nprocs();   //GNU fuction
#else
	return 0; //unknow OS
#endif	
}



