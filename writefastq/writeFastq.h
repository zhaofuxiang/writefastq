#ifndef WRITE_FASTQ_H
#define WRITE_FASTQ_H
#pragma once

#include "dataStructure.h"
#include "Barcode.h"

class WriteFastq
{
public:
	WriteFastq(const fastqParameters& fqPara); // just pass the parameters
	~WriteFastq();

public:
	int init(unsigned int cal_size); // malloc memory etc.

	void getData(string filename, int col, int row); // a thread for read data, it can load data to m_info and process(matrix transpose and split barcode), then push the data to m_buffer;

	void outData();

	int endFastq();

private:
	int createOutputFiles(string prefix);
	int readAllCycleCal(const string fileName, DataInfo *buff);
	int transformData(DataInfo *info);
	int getFastq(DataInfo *info);

public:
	bool process_finished;

private:
	DataInfo *m_info;
	vector<vector<string> > m_buffer;
	vector<mutex *> m_public_buff_mutexs;
	// fix me: replace with gz file
	vector<vector<FILE *>> m_fastq_files;

	// barcodeInfo barcode_info;

	u_short read1_length;
	u_short read2_length;

	u_short thread_number; // the max cpu number the user wanted
	u_short memory; // the max memory the user wanted, unit is (GB)

	string out_path; // output fastq directory

	string read_prefix; // like: FS2000L1 (slide+lane), used for out fastq

	u_short data_mode; // 0->read data from cal files 1->read data from fastq files
	// calInfo cal_info;
	// fastqInfo fastq_info;

	u_short filter_mode; // 0->no filter 1->do filter others means specific filter method
	u_short filter_quality_read1; // filter quality threshold
	u_short filter_quality_read2;
	string cal_path;
	vector<fovInfo> fov_list;

	u_short end_cycle_process_read1; // process for end cycle, so ignore end cycle
	u_short end_cycle_process_read2;

	bool use_gpu; // true: use gpu false: use cpu
	unsigned int cal_size;


	Barcode m_barcode;

#ifdef COMPILE_GPU
	mutex *m_gpu_mutex;
#endif
	mutex *m_get_data_mutex;
};

#endif // WRITE_FASTQ_H