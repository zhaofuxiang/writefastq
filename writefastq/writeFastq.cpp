
#include "writeFastq.h"

#include <boost/filesystem.hpp>

#include <iostream>


#include "Timer.h"

#include "gpuWriteFq/writeFqGPU.cuh"

#include <thread>
using namespace std;

WriteFastq::WriteFastq(const fastqParameters& fqPara)
{
	read1_length = fqPara.read1_length;
	read2_length = fqPara.read2_length;

	thread_number = fqPara.thread_number; 
	memory = fqPara.memory;

	out_path = fqPara.out_path;

	read_prefix = fqPara.read_prefix;

	data_mode = fqPara.data_mode;

	filter_mode = fqPara.cal_info.filter_mode;
	filter_quality_read1 = fqPara.cal_info.filter_quality_read1;
	filter_quality_read2 = fqPara.cal_info.filter_quality_read2;
	cal_path = fqPara.cal_info.cal_path;
	fov_list = fqPara.cal_info.fov_list;

	end_cycle_process_read1 = fqPara.cal_info.end_cycle_process_read1;
	end_cycle_process_read2 = fqPara.cal_info.end_cycle_process_read2;

	use_gpu = fqPara.cal_info.use_gpu;

	process_finished = false;
}

WriteFastq::~WriteFastq()
{
	
}

int WriteFastq::init(unsigned int calsize)
{
	// malloc memory for cal buffer
	cal_size = calsize;
	unsigned int cal_buff_size = (read1_length+read2_length)*(cal_size+sizeof(int));

	m_info = new DataInfo[thread_number];
	for (int i = 0; i < thread_number; ++i)
	{
		m_info[i].data = (unsigned char*)malloc(cal_buff_size * sizeof(unsigned char));
		if (!m_info[i].data)
		{
			cout << "failed malloc data buffer" << endl;
			return -1;
		}
		m_info[i].cycle = 0;
		m_info[i].base_number = 0;
		m_info[i].col = 0;
		m_info[i].row = 0;
		m_info[i].sequence = "";
		m_info[i].score = "";
		m_info[i].data_flag = DataFlag::empty;
		m_info[i].sequence.resize(cal_buff_size);
		m_info[i].score.resize(cal_buff_size);
	}
	// prepare buffer before compress
	int barcode_num = -1;
	m_buffer.resize(barcode_num + 2);
	m_public_buff_mutexs.resize(barcode_num+2);

	for (int i = 0; i < barcode_num + 2; ++i)
	{
		// for read1 and read2
		m_buffer[i].resize(2);

		m_public_buff_mutexs[i] = new mutex;
	}

	createOutputFiles(read_prefix);

	m_gpu_mutex = new mutex;
	m_get_data_mutex = new mutex;
	
#ifdef COMPILE_GPU
	if (use_gpu)
	{
		// int *base2int = m_barcode.getBase2Idx();
		char BASE[] = "ACGTN";
		int base2int[128];
		base2int['N'] = 0;
		base2int['A'] = 1;
		base2int['C'] = 2;
		base2int['G'] = 3;
		base2int['T'] = 4;

		int m_numCycles = read1_length + read2_length;
		int *filterParam = NULL;
		try
		{
			filterParam = new int[3 * (read1_length + read2_length)]();
		}
		catch (std::bad_alloc)
		{
			//CGI_ERROR(log, "lack of memroy for gpu mode, so write fastq in cpu mode");
			use_gpu = false;
		}

		if (filterParam != NULL)
		{
			int *filter_cyc = filterParam, *Qdel = filterParam + m_numCycles, *Fdel = filterParam + 2 * m_numCycles;
			//for (int i = 0; i < m_numCycles; i++) //no barcode merge
			//{
			//	if ((m_filterCycStart <= i) && (i < m_filterCyc))
			//	{
			//		filter_cyc[i] = 1; Qdel[i] = m_filterQ1; Fdel[i] = 2; //read1
			//	}
			//	else if (i == m_filterCycStart2)
			//	{
			//		filter_cyc[i] = 2; Qdel[i] = m_filterQ2; Fdel[i] = 2;
			//	}
			//	else if ((i > m_filterCycStart2) && (i < m_filterCyc2))   //read2
			//	{
			//		filter_cyc[i] = 2; Qdel[i] = m_filterQ2; Fdel[i] = 2;
			//	}
			//}

			//std::vector<int> hashTable = m_barcode.getMyHashTable();
			std::vector<int> hashTable;
			if (initGPUWFqMEM(cal_size, m_numCycles, base2int, filterParam, BASE, hashTable) != 0)
			{
				//CGI_ERROR(log, "initGPUWFqMEM failed. maybe lack of memory or there is no gpu card. so write fastq in cpu mode");
				use_gpu = false;
			}
		}
	}
#endif

	return 0;
}

void WriteFastq::getData(string filename, int col, int row)
{
	Timer timer, t;

	t.tic();
	int data_position;

	m_get_data_mutex->lock();
	while (true)
	{
		data_position = 0;
		for (; data_position < thread_number; ++data_position)
		{
			if (m_info[data_position].data_flag == DataFlag::empty)
			{
				m_info[data_position].data_flag = DataFlag::hold;
				break;
			}
		}
		if (data_position != thread_number)
			break;
		this_thread::sleep_for(chrono::microseconds(10));
	}
	m_get_data_mutex->unlock();

	cout << "C" << col << "R" << row << " pos " << data_position<<endl;
	DataInfo *info = &m_info[data_position];

	int cycle = readAllCycleCal(filename, info);
	info->cycle = cycle;
	info->col = col;
	info->row = row;
		
	cout << "read cal time(s): " << t.toc() << endl;

	if (cycle == (read1_length + read2_length))
	{
		info->data_flag = DataFlag::raw;
		transformData(info);
		info->data_flag = DataFlag::transform;
		cout << "transformData time(s): " << t.toc() << endl;
		getFastq(info);
		info->data_flag = DataFlag::empty;
		cout << "getFastq time(s): " << t.toc() << endl;
	}
	

	cout << "process single fov total time(s): " << timer.toc() << endl;
}

int WriteFastq::transformData(DataInfo *info)
{
	int NDnb = info->base_number;
	int NCyc = read1_length + read2_length;

	int calSize = cal_size + sizeof(int);
	int *h_hashVal = (int*)malloc(calSize * sizeof(int));
	int *h_speciesNo = (int*)malloc(calSize * sizeof(int));

	try
	{
		m_gpu_mutex->lock();
		transGPU(&info->sequence[0], &info->score[0], info->data, NCyc, NDnb, read1_length + filter_quality_read2, h_hashVal, h_speciesNo, -1, 0);
		m_gpu_mutex->unlock();
	}
	catch (...)
	{
		cout << "transGPU error" << endl;
		return -1;
	}
}

int WriteFastq::getFastq(DataInfo *info)
{
	Timer timer;
	std::vector< std::string > fastqBuffs, fastqBuffs2;
	size_t barcodeNum = 1;
	fastqBuffs.resize(barcodeNum);
	fastqBuffs2.resize(barcodeNum);

	/*fastqBuffs[0].reserve(500 * 1024 * 1024);
	fastqBuffs2[0].reserve(500 * 1024 * 1024);*/

	char crName[20];
	sprintf(crName, "C%03dR%03d", info->col, info->row);
	std::string readName = read_prefix + crName;

	timer.tic();
	int NDnb = info->base_number;
	int NCyc = read1_length + read2_length;
	// se mode
	if (read1_length == 0 || read2_length == 0)
	{

	}
	else
	{
		char buff[1024];
		char buff2[1024];
		int speciesNO = 0;
		for (int dnb = 0; dnb < NDnb; dnb++)
		{
			
			//int hashVal;
			// fix me: do filter
			/*if (m_ifFilter && h_filterFlag[dnb] > 2)
			{
				noGRR++;	continue;
			}*/

			//try
			//{
			//	// fix me: split barcode
			//	speciesNO = h_speciesNo[dnb];
			//	hashVal = h_hashVal[dnb];
			//	m_barcode.BarcodeInfo(m_info->Seq.substr(dnb*NCyc + m_startBarcode, m_hasBarcode), hashVal);
			//}
			//catch (...)
			//{
			//	//CGI_ERROR(log, "m_barcode.getMySpeciesNO() error: c" << c << " r" << r << "dnb= " << dnb << " barcodeSeq: " << m_info->Seq.substr(dnb*NCyc + m_startBarcode, m_hasBarcode) << " " << flag);
			//	return;
			//}

			sprintf(buff, "@%s%07d/1\n%s\n+\n%s\n", readName.c_str(), dnb, info->sequence.substr(dnb*NCyc, read1_length).c_str(), info->score.substr(dnb*NCyc, read1_length).c_str()); //same with "do barcode"
			sprintf(buff2, "@%s%07d/2\n%s\n+\n%s\n", readName.c_str(), dnb, info->sequence.substr(dnb*NCyc + read1_length, read2_length).c_str(), info->score.substr(dnb*NCyc + read1_length, read2_length).c_str());
			fastqBuffs[speciesNO].append(buff);
			fastqBuffs2[speciesNO].append(buff2);

		}
	}

	cout << "get fq time: " << timer.toc() << endl;
	/*for (int i = 0; i < barcodeNum; ++i)
	{
		m_public_buff_mutexs[i]->lock();

		m_buffer[i][0].append(fastqBuffs[i]);
		m_buffer[i][1].append(fastqBuffs2[i]);

		m_public_buff_mutexs[i]->unlock();
	}
	cout << "push fq time: " << timer.toc() << endl;*/
	return 0;
}

void WriteFastq::outData()
{
	Timer timer, t;
	const size_t out_buff_size = 50 * 1024 * 1024;
	size_t barcodeNum = 1;

	while (true)
	{
		if (process_finished)
			break;

		for (int i = 0; i < barcodeNum; ++i)
		{
			m_public_buff_mutexs[i]->lock();
			// fix me: add se mode
			if (m_buffer[i][0].size() > out_buff_size || m_buffer[i][1].size() > out_buff_size)
			{
				t.tic();
				fwrite(m_buffer[i][0].c_str(), 1, m_buffer[i][0].size(), m_fastq_files[i][0]);
				//fputs(m_buffer[i][0].c_str(), m_fastq_files[i][0]);
				//cout << "write f1 " << t.toc() << endl;
				fwrite(m_buffer[i][1].c_str(), 1, m_buffer[i][1].size(), m_fastq_files[i][1]);
				//cout << "write f2 " << t.toc() << endl;
				m_buffer[i][0].clear();
				m_buffer[i][1].clear();
				cout << "write disk time(s): " << t.toc() << endl;
			}
			m_public_buff_mutexs[i]->unlock();
		}
		this_thread::sleep_for(chrono::microseconds(10));
	}
	//cout << "outData time(s): " << timer.toc() << endl;
}

int WriteFastq::endFastq()
{
	size_t barcodeNum = 1;
	for (int i = 0; i < barcodeNum; ++i)
	{
		fclose(m_fastq_files[i][0]);
		fclose(m_fastq_files[i][1]);
	}

	for (int i = 0; i < thread_number; ++i)
	{
		free(m_info[i].data);
		m_info[i].data = NULL;
	}
	delete[] m_info;

#ifdef COMPILE_GPU
	delete m_gpu_mutex;
	m_gpu_mutex = NULL;
	if (use_gpu)
		destroyGPUWFqMEM;
#endif

	delete m_get_data_mutex;
	m_get_data_mutex = NULL;

	int barcode_num = -1;
	for (int i = 0; i < barcode_num + 2; ++i)
	{
		delete m_public_buff_mutexs[i];
		m_public_buff_mutexs[i] = NULL;
	}
	return 0;
}
int WriteFastq::createOutputFiles(string prefix)
{
	size_t barcodeNum = 1;
	m_fastq_files.resize(barcodeNum);
	for (int i = 0; i < barcodeNum; ++i)
	{
		m_fastq_files[i].resize(2);
		// fix me: add se mode
		string label = "read1";
		string fq_name = out_path + "/" + prefix + "_" + label + ".txt";
		m_fastq_files[i][0] = fopen(fq_name.c_str(), "w");
		if (m_fastq_files[i][0] == NULL)
		{
			cout<< "can not open fastq file to write: " << fq_name << endl;
			return -1;
		}

		label = "read2";
		fq_name = out_path + "/" + prefix + "_" + label + ".txt";
		m_fastq_files[i][1] = fopen(fq_name.c_str(), "w");
		if (m_fastq_files[i][1] == NULL)
		{
			cout << "can not open fastq file to write: " << fq_name << endl;
			return -1;
		}
	}

	return 0;
}

int WriteFastq::readAllCycleCal(const string fileName, DataInfo *buff)
{
	FILE * file;
	file = fopen(fileName.c_str(), "rb");
	if (file == NULL)
	{
		cout << "Error: Cannot open the  one cycle call file to read" << endl;
		return -1;
	}

	int numDNBs, TotalCyc;
	fread(&numDNBs, sizeof(int), 1, file);
	fread(&TotalCyc, sizeof(int), 1, file);

	//buff.cycle = m_cycleNum;
	int tmp = read1_length+read2_length;
	if (end_cycle_process_read1)
	{
		tmp++;
	}
	if (end_cycle_process_read2)
	{
		tmp++;
	}

	if (TotalCyc < tmp) //failed
	{
		std::cout << "error writeFq parameter, setted cycle is bigger than calFile cycle" << std::endl;
		fclose(file);
		return -1;
	}

	// fix me: if real cycle is 100+110, but the cycle parameters is 10+20, the it was wrong

	if (!end_cycle_process_read1 && !end_cycle_process_read2)
	{
		size_t dataSize = (sizeof(int) + numDNBs)*tmp;
		fread(buff->data, sizeof(unsigned char), dataSize, file); //read all data
	}
	else if (end_cycle_process_read1 && !end_cycle_process_read2)
	{
		size_t dataSize1 = (sizeof(int) + numDNBs)*(end_cycle_process_read1 - 1);
		fread(buff->data, sizeof(unsigned char), dataSize1, file);
		fseek(file, sizeof(int) + numDNBs, SEEK_CUR);
		fread(&(buff->data[dataSize1]), sizeof(unsigned char), (sizeof(int) + numDNBs)*(tmp - end_cycle_process_read1), file); //read all data
	}
	else if (!end_cycle_process_read1 && end_cycle_process_read2)
	{
		size_t dataSize1 = 0;
		dataSize1 = (sizeof(int) + numDNBs)*(end_cycle_process_read2 - 1);
		fread(buff->data, sizeof(unsigned char), dataSize1, file);
		fseek(file, sizeof(int) + numDNBs, SEEK_CUR);
		fread(&(buff->data[dataSize1]), sizeof(unsigned char), (sizeof(int) + numDNBs)*(tmp - end_cycle_process_read2), file); //read all data
	}
	else
	{
		size_t dataSize1 = 0, dataSize2 = 0;
		dataSize1 = (sizeof(int) + numDNBs)*(end_cycle_process_read1 - 1);
		fread(buff->data, sizeof(unsigned char), dataSize1, file);
		fseek(file, sizeof(int) + numDNBs, SEEK_CUR);

		dataSize2 = (sizeof(int) + numDNBs)*(end_cycle_process_read2 - end_cycle_process_read1 - 1);
		fread(&(buff->data[dataSize1]), sizeof(unsigned char), dataSize2, file);
		fseek(file, sizeof(int) + numDNBs, SEEK_CUR);
		fread(&(buff->data[dataSize1 + dataSize2]), sizeof(unsigned char), (sizeof(int) + numDNBs)*(tmp - end_cycle_process_read2), file); //read all data
	}

	buff->base_number = numDNBs;

	fclose(file);	
	return read1_length + read2_length;
}