
#include <iostream>
using namespace std;

#include "dataStructure.h"
#include "generateFastq.h"

#include "Timer.h"

int main(int argc, char** argv)
{
	fastqParameters fqPara;
	fqPara.cal_info.cal_path = "D:/ZFX/git/writefastq/test/calFile";
	fqPara.cal_info.end_cycle_process_read1 = 0;
	fqPara.cal_info.end_cycle_process_read2 = 0;
	fqPara.cal_info.filter_mode = 0;
	fqPara.cal_info.filter_quality_read1 = 15;
	fqPara.cal_info.filter_quality_read2 = 15;
	fqPara.cal_info.use_gpu = true;
	fqPara.cal_info.fov_list.clear();
	
	fqPara.data_mode = 0;
	fqPara.memory = 10;
	fqPara.thread_number = 5;
	fqPara.out_path = "D:/ZFX/git/writefastq/test/result";
	fqPara.read1_length = 100;
	fqPara.read2_length = 110;
	fqPara.read_prefix = "v8L1";

	int c = 5;
	for (int r = 17; r < 17+ fqPara.thread_number; ++r)
	{
		fovInfo fov_info;
		fov_info.col = c;
		fov_info.row = r;
		fqPara.cal_info.fov_list.push_back(fov_info);
	}

	Timer timer;
	int rtn = generateFastq(fqPara);
	cout << "generateFastq return code: " << rtn <<" time(s): "<<timer.toc() << endl;

	return 0;
}