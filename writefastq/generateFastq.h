#ifndef GENERATE_FASTQ_H
#define GENERATE_FASTQ_H
#pragma once

#include "dataStructure.h"

int generateFastq(const fastqParameters& fqPara);

#endif // GENERATE_FASTQ_H
