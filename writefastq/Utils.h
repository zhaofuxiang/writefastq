//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <vector>

#ifdef _WIN32
#include <Windows.h>
#include <direct.h>
#else
#include <unistd.h>
#include <sys/sysinfo.h>
#endif

#define mylong long long

std::vector<std::string> split_string(std::string & s, std::string sep);
void split(const std::string &str, char delim, std::vector<std::string> &elems, bool skip_empty = true);
std::string replace_string(std::string s, const std::string k, const std::string v);
bool create_directory(std::string &d);
mylong getMemoryInfo(mylong &totalPhysMem, mylong &validPhysMem);
int get_CPU_core_num();


#endif // UTILS_H
