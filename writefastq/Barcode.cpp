//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#include "Barcode.h"
#include <vector>
#include <algorithm>

#include "Utils.h"

bool descending_order(const pair<string, long long> a, const pair<string, long long> b)
{
    return a.second > b.second;
}

void getMisSequence(string s, int m, vector<string>& r, string replace)
{
    if (m <= 0)
    {
        vector<string>::iterator it = std::find(r.begin(),r.end(), s);
        if (it == r.end())
            r.push_back(s);
        return;
    }
    for (int i = 0; i < s.size(); ++i)
        for (int j = 0; j < replace.size(); ++j)
        {
            string new_s = s.substr(0,i) + replace[j] + s.substr(i+1,s.size());
            getMisSequence(new_s, m-1, r, replace);
        }
}

Barcode::Barcode(std::string &barcodeFile)
{
    lens = 0;
    count = 0;
	barcodes.clear();
    load(barcodeFile);

	if (barcodes.size() != 0)
		lens = barcodes[0].first.size();

    m_barcodeInfoLock = new mutex;
}

Barcode::~Barcode()
{

}

void Barcode::reverseComplement()
{
    for (int i = 0; i < barcodes.size(); ++i)
    {
        string tmp_barcode = barcodes[i].first;
        barcodes[i].first = replace_string(tmp_barcode, "ACGT", "TGCA");
    }
}

void Barcode::createMisDict(vector<int> & barcode_info)
{
#ifdef DEBUG
    for (vector<pair<string,string>>::const_iterator it=barcodes.begin();it!=barcodes.end();++it)
        cout<<"first "<<it->first<<" second "<<it->second<<endl;
#endif

    string replace("ACGTN");
    vector<string> sequence1;
    vector<string> sequence2;
    vector<string> sequence;
    for (int i = 0; i < barcodes.size(); ++i)
    {
        sequence1.clear();
        sequence2.clear();
        sequence.clear();
        getMisSequence(barcodes[i].first.substr(0, barcode_info[1]), barcode_info[2], sequence1, replace);
		if (barcode_info.size() == 6)
			getMisSequence(barcodes[i].first.substr(barcode_info[1], barcode_info[1]+ barcode_info[4]), barcode_info[5], sequence2, replace);
        for (vector<string>::const_iterator it1 = sequence1.begin(); it1 != sequence1.end(); ++it1)
            for (vector<string>::const_iterator it2 = sequence2.begin(); it2 != sequence2.end(); ++it2)
            {
                sequence.push_back(*it1+*it2);
            }

		if (barcode_info.size() == 6)
			makeHash(sequence, i + 1, mis_dict);
		else
			makeHash(sequence1, i + 1, mis_dict);
    }
#ifdef DEBUG
    cout<<"mis_dict size "<<mis_dict.size()<<endl;
#endif
    //cout<<mis_dict["TAGGTCCGATTAGGTCCGAT"]<<endl;
    //cout<<mis_dict["AAGGTCCGATTAGGTCCGAT"]<<endl;
    //cout<<mis_dict["AAGGTCCGATTAGGTCCGAN"]<<endl;
    //cout<<mis_dict["ACGGTCCGATTAGGTCCGAN"]<<endl;
    //cout<<mis_dict["AAGGTCCGATTAGGTCCGCN"]<<endl;
}

void Barcode::makeHash(vector<string>& sequence, int species, unordered_map<string,int>& hash_table)
{
    string s;
    for (vector<string>::const_iterator it = sequence.begin(); it != sequence.end(); ++it)
    {
        s = *it;
        if (hash_table.count(s) == 0)
            hash_table[s] = species;
        else
            hash_table[s] = -1;
    }
}

void Barcode::load(string & barcodeFile)
{
    barcodes.clear();

    FILE * file;
    file = fopen(barcodeFile.c_str(), "r");
    if (file == NULL)
    {
        cout<<"barcodeFile: "<<barcodeFile<<" error!"<<endl;
        return;
    }

    char buff[300];
    char barcode[100];
    char species[100];
    m_barcode_num = 0;
    while (fgets(buff, 300, file))
    {
        if (buff[0] == '#')
            continue;
        if (2 != sscanf(buff, "%s %s", species, barcode))
            continue;
        barcodes.push_back(make_pair(barcode, species));
        m_barcode_num ++;
    }

    fclose(file);
    return;
}

vector<string> Barcode::getBarcodes()
{
    vector<string> b;
    for (vector<pair<string,string>>::const_iterator it = barcodes.begin(); it != barcodes.end(); ++it)
    {
        b.push_back(it->second);
    }
    return b;
}

int Barcode::searchBarcdeNumber(string & seq)
{
    if (seq.size() == 0)
        return 0;
    
    int species = 0;
    if (mis_dict.find(seq) != mis_dict.end())
        species = mis_dict[seq];
	int rtn = species;
    if (species == -1)
        rtn = m_barcode_num + 1;

    m_barcodeInfoLock->lock();
    if (m_barcodeInfo.find(seq) == m_barcodeInfo.end())
    {
        //pair tmp = make_pair<rtn, 1>;
        m_barcodeInfo[seq] = make_pair(rtn, (long long)1);
    }
    else
        m_barcodeInfo[seq].second++;
    m_barcodeInfoLock->unlock();

    return rtn;
}

int Barcode::statInfo(const string & path)
{
    FILE *BarcodeStat, *SequenceStat;   
    BarcodeStat  = fopen((path+"/BarcodeStat.txt").c_str(), "w");
    SequenceStat = fopen((path+"/SequenceStat.txt").c_str(),"w");
    if(!BarcodeStat || !SequenceStat)
    {
        printf("ERROR: fail to open the barcode statistic file: %s\n", (path + "/BarcodeStat.txt").c_str());
        if (BarcodeStat != NULL)
            fclose(BarcodeStat);
        if (SequenceStat != NULL)
            fclose(SequenceStat);
        return -1;
    }

    // dump barcode stat file
    unordered_map<int, long long> barcode_reads;
    unordered_map<string, pair<int, long long> >::const_iterator it;
    vector<pair<string, long long> > temp_count;
    int barcode_index;
    long long times;
    long long total_reads = 0;
    long long total_barcode_reads = 0;
    for (it = m_barcodeInfo.begin(); it != m_barcodeInfo.end(); ++it)
    {
        barcode_index = it->second.first;
        times = it->second.second;
        if (barcode_reads.find(barcode_index) == barcode_reads.end())
            barcode_reads[barcode_index] = times;
        else
            barcode_reads[barcode_index] += times;
        total_reads += times;
        if (barcode_index != 0 && barcode_index != m_barcode_num+1)
            total_barcode_reads += times;

        temp_count.push_back(make_pair(it->first, times));
    }

    fprintf(BarcodeStat,"#Barcode\t Correct\t Corrected\t Total\t Percentage(%%)\n");
    long acc_exactCount=0, acc_correctedCount=0;
    for (int i = 0; i < barcodes.size(); ++i)
    {
        string sequence = barcodes[i].first;
        string barcode = barcodes[i].second;
        long long exactCount = m_barcodeInfo[sequence].second;
        long long totalCount;
        if (barcode_reads.find(i+1) != barcode_reads.end())
            totalCount = barcode_reads[i + 1];
        else
            totalCount = 0;
        long long correctedCount = totalCount - exactCount;
		if (correctedCount < 0)
		{
			cout << "barcode stat error" << endl;
			continue;
		}
        float percent = (1.0*totalCount / total_reads)*100;
        fprintf(BarcodeStat,"barcode%s\t %llu\t %llu\t %llu\t %f\n", barcode.c_str(),exactCount,correctedCount, totalCount, percent);

        acc_exactCount += exactCount;
        acc_correctedCount += correctedCount;
    }
	if (total_reads > 0)
	{
    float acc_percent = (1.0*total_barcode_reads / total_reads)*100;
    fprintf(BarcodeStat,"Total\t %llu\t %llu\t %llu\t %f\n", acc_exactCount,acc_correctedCount, total_barcode_reads, acc_percent);   
	}

    // dump sequence stat file
    std::sort(temp_count.begin(), temp_count.end(), descending_order);

    fprintf(SequenceStat,"#Sequence\t Barcode\t Count\t Percentage(%%)\n");
    for(int i=0; i < m_barcodeInfo.size(); ++i)
    {
        string sequence = temp_count[i].first;
        long long read_count = temp_count[i].second;
        int barcode = m_barcodeInfo[sequence].first;
        if(barcode == 0)
            fprintf(SequenceStat, "%s\t undecoded\t %llu\t %f\n", sequence.c_str(), read_count, (1.0*read_count/total_reads)*100);

        else if(barcode == m_barcode_num+1)
            fprintf(SequenceStat, "%s\t ambiguous\t %llu\t %f\n", sequence.c_str(), read_count, (1.0*read_count/total_reads)*100);
        else
        {
            barcode = atoi(barcodes[barcode-1].second.c_str());
            fprintf(SequenceStat, "%s\t barcode%d\t %llu\t %f\n", sequence.c_str(), barcode, read_count, (1.0*read_count / total_reads) * 100);
        }
    }


    fclose(BarcodeStat);
    fclose(SequenceStat);
}
